import java.util.*;

import javax.swing.event.SwingPropertyChangeSupport;

abstract class Movie {
    private String title;
    private String director;
    private int year;

    void setTitle(String title){
        this.title = title;
    }
    String getTitle(){
        return this.title;
    }
    void setDirector (String director){
        this.director = director;
    }
    String getDirector (){
        return this.director;
    }
    void setYear(int year){
        this.year = year;
    }
    int getYear(){
        return this.year;
    }
    
}
class ShowMovieDetails extends Movie{
    private String genre;
    private String cast;

    void setGenre(String genre){
       this.genre = genre;
    }
    String getGenre(){
        return this.genre;
    }
    void setCast(String cast){
        this.cast = cast;
    }
     String getCast(){
         return this.cast;
    }
    void showMovieinList(){
        System.out.println(" "+this.getTitle()+"    ("+this.getYear()+")");
    }
    void ShowMovie () {
    System.out.println("\nMOVIE DETAILS\n");
    System.out.println("Title : "+this.getTitle());
    System.out.println("Director : "+this.getDirector());
    System.out.println("Year : "+this.getYear());
    System.out.println("Genre : "+this.getGenre());
    System.out.println("Cast : "+this.getCast());
    }
}
class ShowSeriesDetails extends Movie{
    private int season, eps;
    private String cast, genre;

    void setSeason(int season){
        this.season = season;
    }
    int getSeason(){
        return this.season;
    }
    void setEps(int eps){
        this.eps = eps;
    }
    int getEps(){
        return this.eps;
    }
    void setGenre(String genre){
       this.genre = genre;
    }
    String getGenre(){
        return this.genre;
    }
    void setCast(String cast){
        this.cast = cast;
    }
     String getCast(){
         return this.cast;
    }
    void showSeriesinList(){
        System.out.println(" "+this.getTitle()+"    ("+this.getYear()+")");
    }

    void ShowSeries () {
    System.out.println("\nSERIES DETAILS\n");
    System.out.println("Title : "+this.getTitle());
    System.out.println("Director : "+this.getDirector());
    System.out.println("Year : "+this.getYear());
    System.out.println("Episodes : "+this.getEps());
    System.out.println("Genre : "+this.getGenre());
    System.out.println("Cast : "+this.getCast());
    }
}

class insertMovieData {

}
class Header {
    void showHeader(){
    System.out.println("\n= = = = ========== = = = =  ");
    System.out.println("        Netflix KW  ");
    System.out.println("= = = = ========== = = = =  ");
    System.out.println("\n    WELCOME!!!\n\n");
    }
    }


public class percobaan1 {
    public static void main(String[] args) {
        int pil1 ,pil2;
        Scanner input = new Scanner(System.in);

        ShowMovieDetails m1 = new ShowMovieDetails();
        m1.setTitle("The Godfather I");
        m1.setDirector("Francis Ford");
        m1.setYear(1985);
        m1.setGenre("Action, Crime");
        m1.setCast("Al Pacino, Marlon Brando, James Caan, Diane Keaton");

        ShowMovieDetails m2 = new ShowMovieDetails();
        m2.setTitle("Harry Potter and the Prisoner of Azkaban"); 
        m2.setDirector("David Yates");
        m2.setYear(1998);
        m2.setGenre("Action, Supranatural, Fantasy");
        m2.setCast("Daniel Radcliffe, Emma Watson");

        ShowSeriesDetails s1 = new ShowSeriesDetails();
        s1.setTitle("Narcos");
        s1.setDirector("Chris Brancato, Carlo Bernard, and Doug Miro");
        s1.setYear(2017);
        s1.setEps(30);
        s1.setSeason(3);
        s1.setGenre("Crime, Action");
        s1.setCast("Wagner Moura, Pedro Pascal, Boyd Hooolbrook");

        ShowSeriesDetails s2 = new ShowSeriesDetails();
        s2.setTitle("The Umbrella Academy");
        s2.setDirector("Steven Balckman");
        s2.setYear(2019);
        s2.setEps(30);
        s2.setSeason(3);
        s2.setGenre("Action, Fantasy, Supranatural");
        s2.setCast("Aidan Gallagher, Elliot Page, Tom Hooper");

        Header header = new Header();
        header.showHeader();
        
        System.out.println("    MENU    ");
        System.out.println("\n 1. Lihat list Film");
        System.out.println(" 2. Lihat list Series");
        System.out.print("Pilihan anda : ");
        pil1 = input.nextInt();
        switch (pil1){
            case 1 : 
                System.out.print("\n1. ");m1.showMovieinList();
                System.out.print("2. ");m2.showMovieinList();   
                System.out.print("\nPilih film untuk melihat detail  ");
                pil2 = input.nextInt();
                switch(pil2){
                    case 1 : m1.ShowMovie();
                    break;
                    case 2 : m2.ShowMovie();
                    break;
                    default :
                    break;
                } 
                break;
            case 2 : 
                System.out.print("\n1. ");s1.showSeriesinList();
                System.out.print("\n2. ");s2.showSeriesinList();
                System.out.print("\nPilih series untuk melihat detail  ");
                pil2 = input.nextInt();
                switch(pil2){
                    case 1 : s1.ShowSeries();
                    break;
                    case 2 : s2.ShowSeries();
                    break;
                    default :
                    break;
                } 
                break;
            default : 
                break;
        }
    }
}